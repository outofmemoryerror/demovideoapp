package com.demo.demovideoapp.model

import java.io.Serializable
import java.util.*

data class Video (
        var id: String?,
        var url: String?,
        var name: String?,
        var author: String?,
        var description: String?,
        var thumb_url: String?,
        var ts: Long?,
        var comments: ArrayList<Comment>?
) : Serializable
