package com.demo.demovideoapp.model

data class Comment (
        var id: String?,
        var text: String?,
        var ts: Long?,
        var author: String?
)