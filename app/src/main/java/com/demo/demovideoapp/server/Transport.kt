package com.demo.demovideoapp.server

import android.text.TextUtils
import android.util.Log
import com.demo.demovideoapp.BuildConfig
import com.demo.demovideoapp.server.models.TokenResponse
import com.demo.demovideoapp.server.models.VideoDetailsResponse
import com.demo.demovideoapp.server.models.VideoListResponse
import com.demo.demovideoapp.utils.SPStorage
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.IOException


object Transport {
    private const val AUTH_HEADER = "Authorization"

    interface TransportService {
        @POST("/login")
        fun loginObservable (
                @Query("email") email: String,
                @Query("password") password: String
        ): Observable<TokenResponse>

        @GET("/videos")
        fun getVideoListObservable (
                @Query("page") page: Int,
                @Query("searchQuery") searchQuery: String
        ): Observable<VideoListResponse>

        @GET("/video/{id}/")
        fun getVideoObservable (
                @Path("id") id: String
        ): Observable<VideoDetailsResponse>

    }

    fun newInstance(): TransportService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(HeadersInterceptor())
                .build()
        val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        return retrofit.create(TransportService::class.java)
    }

    //Adding token to header if it is not empty
    private class HeadersInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            var request = chain.request()
            val token = SPStorage.getToken()
            val builder = request.newBuilder()
            if (!TextUtils.isEmpty(token)) {
                if (BuildConfig.DEBUG) {
                    Log.d("OkHttp", "$AUTH_HEADER $token")
                }
                builder.addHeader(AUTH_HEADER, SPStorage.getToken())
            }
            request = builder.build()
            return chain.proceed(request)
        }
    }
}