package com.demo.demovideoapp.server.models

import com.demo.demovideoapp.model.Video

class VideoDetailsResponse(val video: Video? = null) : ErrorResponse()