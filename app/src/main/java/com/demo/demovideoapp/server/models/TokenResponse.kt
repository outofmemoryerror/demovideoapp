package com.demo.demovideoapp.server.models

class TokenResponse(val token: String? = null) : ErrorResponse()