package com.demo.demovideoapp.server.models

import android.text.TextUtils

open class ErrorResponse(
        var error_code: String? = null,
        var error_message: String? = null
) {
    val isSuccessful: Boolean
        get() = TextUtils.isEmpty(error_code)
}