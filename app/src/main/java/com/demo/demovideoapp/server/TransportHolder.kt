package com.demo.demovideoapp.server


class TransportHolder {
    companion object {
        private var mInstance: Transport.TransportService = Transport.newInstance()
        @Synchronized
        fun getInstance(): Transport.TransportService {
            return mInstance
        }
    }
}