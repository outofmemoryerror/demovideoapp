package com.demo.demovideoapp.server.models

import com.demo.demovideoapp.model.Video

class VideoListResponse(val results: ArrayList<Video>? = null) : ErrorResponse()