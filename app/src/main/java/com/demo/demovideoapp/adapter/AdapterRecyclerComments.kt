package com.demo.demovideoapp.adapter

import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.demo.demovideoapp.R
import com.demo.demovideoapp.activity.ActivityVideo
import com.demo.demovideoapp.model.Comment
import com.demo.demovideoapp.model.Video
import com.demo.demovideoapp.utils.Utils
import java.util.*


class AdapterRecyclerComments(var video: Video?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_TYPE_HEADER = -1
    private val VIEW_TYPE_LIST = 0
    private val comments = ArrayList<Comment>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_LIST -> return ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))
        }
        return HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.header_video_info, parent, false))
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_LIST -> getItem(position)?.let { (holder as ListViewHolder).bindItem(it) }
            VIEW_TYPE_HEADER -> video?.let { (holder as HeaderViewHolder).bindItem(it) }
        }

    }

    fun clear() {
        comments.clear()
        notifyDataSetChanged()
    }

    fun addAll(toAdd: List<Comment>) {
        comments.addAll(toAdd)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Comment? {
        return if (position == 0) {
            null
        } else {
            comments[position - 1]
        }
    }

    override fun getItemCount(): Int {
        return comments.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            VIEW_TYPE_HEADER
        } else {
            VIEW_TYPE_LIST
        }
    }

    private inner class ListViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
        internal var author: TextView
        internal var text: TextView
        internal var date: TextView

        init {
            author = rowView.findViewById(R.id.author)
            text = rowView.findViewById(R.id.text)
            date = rowView.findViewById(R.id.date)

        }

        fun bindItem(item: Comment) {
            author.text = item.author
            text.text = item.text
            date.text = Utils.getDateString(item.ts)
        }
    }

    private inner class HeaderViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
        internal var name: TextView
        internal var description: TextView
        internal var date: TextView

        init {
            name = rowView.findViewById(R.id.name)
            description = rowView.findViewById(R.id.description)
            date = rowView.findViewById(R.id.date)

        }

        fun bindItem(item: Video) {
            name.text = item.name
            description.text = item.description
            date.text = Utils.getAutorWithDate(item)
        }
    }
}
