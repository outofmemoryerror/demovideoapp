package com.demo.demovideoapp.adapter

import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.demo.demovideoapp.R
import com.demo.demovideoapp.activity.ActivityVideo
import com.demo.demovideoapp.model.Video
import com.demo.demovideoapp.utils.Utils
import java.util.*


class AdapterRecyclerVideo(
        private var mLayoutManager: RecyclerView.LayoutManager?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_EMPTY = -1
    private val VIEW_TYPE_LIST = 0
    private val VIEW_TYPE_GRID = 1
    private val videos = ArrayList<Video>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_LIST -> return ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_video_list, parent, false))
            VIEW_TYPE_GRID -> return GridViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_video_grid, parent, false))
        }
        return EmptyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_empty, parent, false))
    }

    fun changeLayoutManager(layoutManager: RecyclerView.LayoutManager) {
        mLayoutManager = layoutManager
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            VIEW_TYPE_LIST -> getItem(position)?.let { (holder as ListViewHolder).bindItem(it) }
            VIEW_TYPE_GRID -> getItem(position)?.let { (holder as GridViewHolder).bindItem(it) }
        }

    }

    fun clear() {
        videos.clear()
        notifyDataSetChanged()
    }

    fun addAll(toAdd: List<Video>) {
        videos.addAll(toAdd)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Video? {
        return if (position >= videos.size) null else videos[position]
    }

    override fun getItemViewType(position: Int): Int {
        return if (videos.isEmpty()) VIEW_TYPE_EMPTY else if (mLayoutManager is GridLayoutManager) VIEW_TYPE_GRID else VIEW_TYPE_LIST
    }

    override fun getItemCount(): Int {
        return if (videos.isEmpty()) 1 else videos.size
    }

    private inner class ListViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
        internal var image: ImageView
        internal var name: TextView
        internal var description: TextView
        internal var date: TextView

        init {
            image = rowView.findViewById(R.id.image)
            name = rowView.findViewById(R.id.name)
            description = rowView.findViewById(R.id.description)
            date = rowView.findViewById(R.id.date)

        }

        fun bindItem(item: Video) {
            name.text = item.name
            description.text = item.description
            date.text = Utils.getAutorWithDate(item)
            Glide
                    .with(itemView.context)
                    .load(item.thumb_url)
                    .into(image)
            //do it here for transaction effect
            itemView.setOnClickListener {
                val intent = Intent(it.context, ActivityVideo::class.java)
                intent.putExtra("video", item)
                it.context.startActivity(intent, Utils.getTransactionBundle(it.context, image, it.context.getString(R.string.splash_transaction)))
            }
        }
    }

    private inner class GridViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
        internal var image: ImageView
        internal var name: TextView
        internal var date: TextView

        init {
            image = rowView.findViewById(R.id.image)
            name = rowView.findViewById(R.id.name)
            date = rowView.findViewById(R.id.date)

        }

        fun bindItem(item: Video) {
            name.text = item.name
            date.text = Utils.getAutorWithDate(item)
            Glide
                    .with(itemView.context)
                    .load(item.thumb_url)
                    .into(image)
            //do it here for transaction effect
            itemView.setOnClickListener {
                val intent = Intent(it.context, ActivityVideo::class.java)
                intent.putExtra("video", item)
                it.context.startActivity(intent, Utils.getTransactionBundle(it.context, image, it.context.getString(R.string.splash_transaction)))
            }
        }
    }

    protected inner class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
