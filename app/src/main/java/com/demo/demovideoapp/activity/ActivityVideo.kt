package com.demo.demovideoapp.activity

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.demo.demovideoapp.R
import com.demo.demovideoapp.adapter.AdapterRecyclerComments
import com.demo.demovideoapp.model.Video
import com.demo.demovideoapp.server.TransportHolder
import com.demo.demovideoapp.utils.Utils
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_video_details.*


class ActivityVideo : AppCompatActivity() {
    private var oldParams: RelativeLayout.LayoutParams? = null
    private var oldParamsImage: RelativeLayout.LayoutParams? = null
    private var video: Video? = null
    private var player: SimpleExoPlayer? = null
    private var adapter: AdapterRecyclerComments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_details)
        video = intent.getSerializableExtra("video") as Video
        Glide
                .with(this)
                .load(video?.thumb_url)
                .into(image)
        video?.id?.let { loadDetails(it) }
        comments.layoutManager = LinearLayoutManager(this)
        adapter = AdapterRecyclerComments(video)
        comments.adapter = adapter
        playButton.setOnClickListener {
            startPlayer()
        }
    }

    private fun pausePlayer() {
        player?.playWhenReady = false
    }

    private fun startPlayer() {
        player?.playWhenReady = true
    }


    private fun play(url: String) {
        la_video.visibility = View.VISIBLE
        progressBar.visibility = View.VISIBLE
        playButton.visibility = View.GONE
        image.visibility = View.GONE
        oldParams = la_video.layoutParams as RelativeLayout.LayoutParams?
        oldParamsImage = image.layoutParams as RelativeLayout.LayoutParams?
        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        val loadControl = DefaultLoadControl() as LoadControl

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl)
        simpleExoPlayerView.useController = true
        simpleExoPlayerView.requestFocus()
        simpleExoPlayerView.player = player
        val mp4VideoUri = Uri.parse(url)
        val bandwidthMeterA = DefaultBandwidthMeter()
        var extractorsFactory = DefaultExtractorsFactory()
        var dataSourceFactory = DefaultHttpDataSourceFactory(Util.getUserAgent(this, BuildConfig.APPLICATION_ID), bandwidthMeterA)
        var videoSource = ExtractorMediaSource(mp4VideoUri, dataSourceFactory, extractorsFactory, null, ExtractorMediaSource.EventListener {
            it.printStackTrace()
            Utils.showMessage(this, it.message)
        })
        var loopingSource = LoopingMediaSource(videoSource)
        player?.prepare(loopingSource)
        player?.addListener(object : Player.EventListener {
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

            }

            override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {

            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                Utils.showMessage(this@ActivityVideo, error?.message)
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_READY) {
                    if (playWhenReady) {
                        image.visibility = View.GONE
                        progressBar.visibility = View.GONE
                        playButton.visibility = View.GONE
                    } else {
                        image.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        playButton.visibility = View.VISIBLE
                    }
                } else {
                    image.visibility = View.VISIBLE
                    progressBar.visibility = View.VISIBLE
                    playButton.visibility = View.GONE
                }
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                image.visibility = if (isLoading) View.VISIBLE else View.GONE
                progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
            }

            override fun onPositionDiscontinuity() {

            }

            override fun onRepeatModeChanged(repeatMode: Int) {

            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {

            }

        })

        player?.playWhenReady = true
    }

    private fun enableFullScreen() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        la_video.layoutParams = params
        image.layoutParams = params
        comments.visibility = View.GONE
    }

    private fun disableFullScreen() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        la_video.layoutParams = oldParams
        image.layoutParams = oldParamsImage
        comments.visibility = View.VISIBLE
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun loadDetails(id: String) {
        progressBar.visibility = View.VISIBLE
        TransportHolder.getInstance()
                .getVideoObservable(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            video = it.video
                            adapter?.clear()
                            video?.comments?.let { it1 -> adapter?.addAll(it1) }
                            video?.url?.let { it1 -> play(it1) }
                        },
                        onError =  {
                            progressBar.visibility = View.GONE
                            Utils.showMessage(this, it.message)
                            it.printStackTrace()
                        },
                        onComplete = {
                            progressBar.visibility = View.GONE
                        }
                )
    }

    //Changing vies size on orientation changes
    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if (newConfig?.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            enableFullScreen()
        } else if (newConfig?.orientation == Configuration.ORIENTATION_PORTRAIT){
            disableFullScreen()
        }
    }

    override fun onPause() {
        super.onPause()
        pausePlayer()

    }
}
