package com.demo.demovideoapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import com.demo.demovideoapp.R
import com.demo.demovideoapp.server.TransportHolder
import com.demo.demovideoapp.server.models.TokenResponse
import com.demo.demovideoapp.utils.Progress
import com.demo.demovideoapp.utils.SPStorage
import com.demo.demovideoapp.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class ActivityLogin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        password.setOnEditorActionListener({ _, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_DONE) {
                checkFields()
            }
            true
        })
        email.addTextChangedListener(textChanger)
        password.addTextChangedListener(textChanger)
        fab.setOnClickListener {
            checkFields()
        }
    }

    private var textChanger = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            fab.visibility = if (Utils.isEmailValid(email.text.toString()) &&
                    Utils.isPasswordValid(password.text.toString())) View.VISIBLE else View.GONE
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    }

    private fun checkFields() {
        if (!Utils.isEmailValid(email.text.toString())) {
            email.error = getString(R.string.email_error)
            return
        }
        if (!Utils.isPasswordValid(password.text.toString())) {
            password.error = getString(R.string.password_error)
            return
        }
        performLogin(email.text.toString().trimIndent(), password.text.toString().trimIndent())
    }

    private fun performLogin(emailField: String, passwordField: String) {
        Progress.showProgress(this)
        TransportHolder.getInstance()
                .loginObservable(emailField, passwordField)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            checkLoginResult(it)
                        },
                        onError =  {
                            Progress.hideProgress()
                            Utils.showMessage(this, it.message)
                            it.printStackTrace()
                        },
                        onComplete = {
                            Progress.hideProgress()
                        }
                )
    }

    private fun checkLoginResult(response: TokenResponse?) {
        if (response != null) {
            if (response.isSuccessful) {
                response.token?.let { SPStorage.saveToken(it) }
                goToMain()
            } else {
                Utils.showMessage(this, response.error_message)
            }
        } else {
            Utils.showMessage(this, getString(R.string.network_error))
        }
    }

    private fun goToMain() {
        val intent = Intent(this, ActivityLanding::class.java)
        startActivity(intent)
    }
}
