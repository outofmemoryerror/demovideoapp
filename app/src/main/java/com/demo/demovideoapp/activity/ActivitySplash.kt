package com.demo.demovideoapp.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import com.demo.demovideoapp.R
import com.demo.demovideoapp.utils.SPStorage
import com.demo.demovideoapp.utils.Utils
import kotlinx.android.synthetic.main.activity_splash.*

class ActivitySplash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            afterDelay()
        }, 1000)
    }

    private fun afterDelay() {
        if (!TextUtils.isEmpty(SPStorage.getToken())) {
            val intent = Intent(this, ActivityLanding::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, ActivityLogin::class.java)
            startActivity(intent, Utils.getTransactionBundle(this, image, getString(R.string.splash_transaction)))
        }
        finishDelay()
    }

    private fun finishDelay() {
        Handler().postDelayed({
            finish()
        }, 1000)
    }

    override fun onBackPressed() {
        //no turning back
    }
}
