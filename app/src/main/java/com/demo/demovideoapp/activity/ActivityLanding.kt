package com.demo.demovideoapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import com.demo.demovideoapp.R
import com.demo.demovideoapp.adapter.AdapterRecyclerVideo
import com.demo.demovideoapp.server.TransportHolder
import com.demo.demovideoapp.server.models.VideoListResponse
import com.demo.demovideoapp.utils.Progress
import com.demo.demovideoapp.utils.SPStorage
import com.demo.demovideoapp.utils.Utils
import com.demo.demovideoapp.widget.EndlessRecyclerOnScrollListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_landing.*
import kotlinx.android.synthetic.main.activity_splash.*
import retrofit2.adapter.rxjava2.HttpException


class ActivityLanding : AppCompatActivity() {
    private var adapter: AdapterRecyclerVideo? = null
    var change: MenuItem? = null
    var searchView: SearchView? = null

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        change = menu.findItem(R.id.change)
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { loadData(1, it) }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })
        initMenuButtons()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.exit -> {
                SPStorage.saveToken("")
                val intent = Intent(this, ActivitySplash::class.java)
                startActivity(intent)
                finish()
                true
            }
            R.id.change -> {
                list.layoutManager = if (list.layoutManager is GridLayoutManager) LinearLayoutManager(this) else GridLayoutManager(this, 2)
                adapter?.changeLayoutManager(list.layoutManager)
                initMenuButtons()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)
        list.layoutManager = LinearLayoutManager(this)
        adapter = AdapterRecyclerVideo(list.layoutManager)
        list.adapter = adapter
        loadData(1, "test")
    }

    private fun loadData(page: Int, text: String) {
        Progress.showProgress(this)
        TransportHolder.getInstance()
                .getVideoListObservable(page, text)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            handleData(page, it)
                        },
                        onError =  {
                            Progress.hideProgress()
                            handleError(page, it)
                        },
                        onComplete = {
                            Progress.hideProgress()
                        }
                )
    }

    private fun handleData(page: Int, it: VideoListResponse) {
        if (page == 1) {
            adapter?.clear()
            list.addOnScrollListener(object : EndlessRecyclerOnScrollListener(list.layoutManager) {
                override fun onLoadMore(current_page: Int) {
                    if (!TextUtils.isEmpty(searchView?.query)) {
                        loadData(current_page, searchView?.query.toString())
                    } else {
                        loadData(current_page, "test")
                    }
                }
            })
            it.results?.let { it1 -> adapter?.addAll(it1) }
        } else {
            it.results?.let { it1 -> adapter?.addAll(it1) }
        }
    }


    private fun handleError(page: Int, it: Throwable) {
        if (it is HttpException) {
            var exc : HttpException = it
            if (404 == exc.code()) {
                if (page == 1) {
                    adapter?.clear()
                }
            } else {
                Utils.showMessage(this, exc.message)
            }
        } else {
            Utils.showMessage(this, it.message)
        }
        it.printStackTrace()
    }

    private fun initMenuButtons() {
        change?.setIcon(if (list.layoutManager is GridLayoutManager) R.drawable.ic_grid_on else R.drawable.ic_view_list)
    }
}
