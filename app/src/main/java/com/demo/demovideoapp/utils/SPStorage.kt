package com.demo.demovideoapp.utils

import android.content.Context
import com.demo.demovideoapp.BuildConfig
import com.demo.demovideoapp.MainApplication

object SPStorage {
    private val TOKEN = "TOKEN"
    private val KEY_STORE_NAME = BuildConfig.APPLICATION_ID

    fun saveToken(token: String) {
        setString(TOKEN, token)
    }

    fun getToken() : String {
        return getString(TOKEN)
    }

    private fun setString(key: String, value: String) {
        val editor = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE).edit()
        editor.putString(key, value)
        editor.commit()
    }

    private fun setInt(key: String, value: Int) {
        val editor = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE).edit()
        editor.putInt(key, value)
        editor.commit()
    }

    private fun setBoolean(key: String, value: Boolean) {
        val editor = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE).edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    private fun getString(key: String): String {
        val settings = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE)
        return settings.getString(key, "")
    }

    private fun getInt(key: String): Int {
        val settings = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE)
        return settings.getInt(key, 0)
    }

    private fun getBoolean(key: String): Boolean {
        val settings = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE)
        return settings.getBoolean(key, true)
    }

    private fun getBooleanFalse(key: String): Boolean {
        val settings = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE)
        return settings.getBoolean(key, false)
    }

    private fun setLong(key: String, value: Long) {
        val editor = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE).edit()
        editor.putLong(key, value)
        editor.commit()
    }

    private fun getLong(key: String): Long {
        val settings = MainApplication.applicationContext().getSharedPreferences(KEY_STORE_NAME, Context.MODE_PRIVATE)
        return settings.getLong(key, 0)
    }
}