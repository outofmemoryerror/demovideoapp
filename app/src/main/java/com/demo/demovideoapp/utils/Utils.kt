package com.demo.demovideoapp.utils

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.view.View
import com.demo.demovideoapp.BuildConfig
import com.demo.demovideoapp.MainApplication
import com.demo.demovideoapp.R
import com.demo.demovideoapp.model.Video
import org.joda.time.*
import java.util.*
import java.util.regex.Pattern


object Utils {
    val UNAUTHORIZED = BuildConfig.APPLICATION_ID + "UNAUTHORIZED"

    fun getTransactionBundle(context: Context, sharedView: View, transitionName: String): Bundle? {
        return ActivityOptionsCompat.makeSceneTransitionAnimation(context as Activity, sharedView, transitionName).toBundle()
    }

    fun isEmailValid(email: String): Boolean {
        var isValid = false
        val pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email.trimIndent())
        if (matcher.matches()) {
            isValid = true
        }
        return isValid
    }

    fun isPasswordValid(password: String) : Boolean {
        return password.trimIndent().length > 5
    }

    fun showMessage(context: Context, message: String?) {
        val mySnackbar = message?.let {
            Snackbar.make((context as Activity).window.decorView.findViewById(android.R.id.content),
                    it, Snackbar.LENGTH_LONG)
        }
        mySnackbar?.show()
    }

    private fun getLastUpdateSuffix(lastUpdate: Date): String {
        return getLastUpdateSuffix(lastUpdate, true)
    }

    private fun getLastUpdateSuffix(lastUpdate: Date, addAgo: Boolean): String {

        val today = DateTime.now().withZone(DateTimeZone.UTC)
        val update = DateTime(lastUpdate).withZoneRetainFields(DateTimeZone.UTC)

        return when {
            Years.yearsBetween(update, today).years > 0 -> getLastUpdateSuffixYear(Years.yearsBetween(update, today).years, addAgo)
            Months.monthsBetween(update, today).months > 0 -> getLastUpdateSuffixMonth(Months.monthsBetween(update, today).months, addAgo)
            Days.daysBetween(update, today).days > 0 -> getLastUpdateSuffixDays(Days.daysBetween(update, today).days, addAgo)
            Hours.hoursBetween(update, today).hours > 0 -> getLastUpdateSuffixHours(Hours.hoursBetween(update, today).hours, addAgo)
            Minutes.minutesBetween(update, today).minutes > 0 -> getLastUpdateSuffixMinutes(Minutes.minutesBetween(update, today).minutes, addAgo)
            Seconds.secondsBetween(update, today).seconds > 0 -> getLastUpdateSuffixSeconds(Seconds.secondsBetween(update, today).seconds, addAgo)
            else -> MainApplication.applicationContext().getString(R.string.now)
        }

    }

    private fun getLastUpdateSuffixDays(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.day)
        }
        else {
            MainApplication.applicationContext().getString(R.string.days)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }

    private fun getLastUpdateSuffixSeconds(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.second)
        }
        else {
            MainApplication.applicationContext().getString(R.string.seconds)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }


    private fun getLastUpdateSuffixMinutes(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.minute)
        }
        else {
            MainApplication.applicationContext().getString(R.string.minutes)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }


    private fun getLastUpdateSuffixHours(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.hour)
        }
        else {
            MainApplication.applicationContext().getString(R.string.hours)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }


    private fun getLastUpdateSuffixYear(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.year)
        }
        else {
            MainApplication.applicationContext().getString(R.string.years)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }

    private fun getLastUpdateSuffixMonth(lastUpdate: Int, addAgo: Boolean): String {
        if (lastUpdate <= 0)
            return MainApplication.applicationContext().getString(R.string.now)
        val result: String
        val a = lastUpdate % 100
        result = if (a == 1) {
            MainApplication.applicationContext().getString(R.string.month)
        }
        else {
            MainApplication.applicationContext().getString(R.string.months)
        }
        return if (addAgo) {
            MainApplication.applicationContext().getString(R.string.ago, result)
        } else ""
    }

    private fun findDaysDiff(lastUpdate: Date): Int {
        val today = DateTime.now().withZone(DateTimeZone.UTC)
        val update = DateTime(lastUpdate).withZoneRetainFields(DateTimeZone.UTC)

        return when {
            Years.yearsBetween(update, today).years > 0 -> Years.yearsBetween(update, today).years
            Months.monthsBetween(update, today).months > 0 -> Months.monthsBetween(update, today).months
            Days.daysBetween(update, today).days > 0 -> Days.daysBetween(update, today).days
            Hours.hoursBetween(update, today).hours > 0 -> Hours.hoursBetween(update, today).hours
            Minutes.minutesBetween(update, today).minutes > 0 -> Minutes.minutesBetween(update, today).minutes
            Seconds.secondsBetween(update, today).seconds > 0 -> Seconds.secondsBetween(update, today).seconds
            else -> 0
        }
    }

    fun getDateString(ts: Long?) : String {
        return if (ts != null) {
            MainApplication.applicationContext().getString(R.string.date_format, Utils.findDaysDiff(Date(ts)).toString(), Utils.getLastUpdateSuffix(Date(ts)))
        } else {
            MainApplication.applicationContext().getString(R.string.now)
        }
    }

    fun getAutorWithDate(video: Video?) : String {
        val ts = video?.ts
        return if (ts != null) {
            var time = MainApplication.applicationContext().getString(R.string.date_format, Utils.findDaysDiff(Date(ts)).toString(), Utils.getLastUpdateSuffix(Date(ts)))
            MainApplication.applicationContext().getString(R.string.uploaded_by, video.author, time)
        } else {
            MainApplication.applicationContext().getString(R.string.now)
        }
    }
}