package com.demo.demovideoapp.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;

import com.demo.demovideoapp.R;

public class CustomMediaController extends MediaController {
    public CustomMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomMediaController(Context context, boolean useFastForward) {
        super(context, useFastForward);
    }

    public CustomMediaController(Context context) {
        super(context);
    }

    private ImageButton mFullScreen;

    @Override
    public void setAnchorView(View view) {
        super.setAnchorView(view);

        mFullScreen = new ImageButton(getContext());
        changeScreen();
        mFullScreen.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity =  Gravity.RIGHT|Gravity.TOP;
        final float scale = getContext().getResources().getDisplayMetrics().density;
        params.setMargins(0, (int) (10 * scale + 0.5f), (int) (5 * scale + 0.5f), 0);
        addView(mFullScreen, params);
        mFullScreen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    setFullScreen(!FullScreen);
                    changeScreen();
                    mListener.onFullScreen(FullScreen);
                }
            }
        });
    }

    private void changeScreen() {
        mFullScreen.setImageResource(FullScreen ? R.drawable.ic_compress : R.drawable.ic_expand);
    }

    private OnFullScreenListener mListener = null;

    public void setOnFullScreenListener(OnFullScreenListener listener) {
        mListener = listener;
    }

    public boolean isFullScreen() {
        return FullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        FullScreen = fullScreen;
        changeScreen();
    }

    private boolean FullScreen = false;

    public interface OnFullScreenListener {
        void onFullScreen(boolean isFullScreen);
    }
}
