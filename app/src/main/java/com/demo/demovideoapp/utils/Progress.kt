package com.demo.demovideoapp.utils

import android.app.AlertDialog
import android.content.Context
import dmax.dialog.SpotsDialog

object Progress {
    private var mProgress: AlertDialog? = null

    fun showProgress(context: Context?) {
        if (mProgress != null && mProgress!!.isShowing) {
            try {
                mProgress!!.hide()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        mProgress = createProgressDialog(context)
    }

    private fun createProgressDialog(mContext : Context?): AlertDialog? {
        return try {
            val dialog = SpotsDialog(mContext)
            dialog.setCancelable(false)
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            dialog
        } catch (ex: Exception) {
            null
        }

    }

    fun hideProgress() {
        if (mProgress != null) {
            try {
                mProgress!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            mProgress = null
        }
    }
}