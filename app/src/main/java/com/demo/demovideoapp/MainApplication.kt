package com.demo.demovideoapp

import android.app.Application
import android.content.Context
import net.danlew.android.joda.JodaTimeAndroid

class MainApplication : Application() {
    init {
        instance = this
    }

    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
    }
}